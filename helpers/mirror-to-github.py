import os
import sys
import yaml
import github
import argparse
import subprocess

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Mirrors a repository up to GitHub.com based on the information in our repo-metadata')
parser.add_argument('--metadata-path', help='Path to the metadata to check', required=True)
parser.add_argument('--organization', help='GitHub Organization to mirror up to', required=True)
parser.add_argument('--token', help='User token to use when authenticating with GitHub.com', required=True)
args = parser.parse_args()

# Make sure our environment is suitable
if 'GL_PROJECT_PATH' not in os.environ:
    print("Unable to determine the repository we are running in - aborting!")
    sys.exit(1)

# Make sure the metadata exists
if not os.path.exists( args.metadata_path ):
    print("The metadata path specified does not exist - aborting!")
    sys.exit(1)

# Prepare to load our metadata
knownProjects = {}

# Start going over the location in question...
for currentPath, subdirectories, filesInFolder in os.walk( args.metadata_path, topdown=False, followlinks=False ):
    # Do we have a metadata.yaml file?
    if 'metadata.yaml' not in filesInFolder:
        # We're not interested then....
        continue

    # Now that we know we have something to work with....
    # Lets load the current metadata up
    metadataPath = os.path.join( currentPath, 'metadata.yaml' )
    metadataFile = open( metadataPath, 'r', encoding='utf8' )
    metadata = yaml.safe_load( metadataFile )

    # Determine where this project lives
    # This turns /home/git/repo-metadata/projects-invent/groupname/projectname into groupname/projectname
    repositoryPath = currentPath[len(args.metadata_path):]

    # Save it to our list of known projects
    knownProjects[ repositoryPath ] = metadata

# Now that we know about all the projects which we are allowed to mirror up, we need to determine which project we are dealing with here
projectPath    = os.environ['GL_PROJECT_PATH']

# Make sure it is one that is registered in the metadata
if projectPath not in knownProjects:
    print("No need to sync to GitHub - not a mainline project: " + projectPath)
    sys.exit(0)

# Also make sure that it isn't a sysadmin/ or websites/ repository, as we're not replicating those to GitHub
# The only exception here is sysadmin/dummy as we need that for testing
if projectPath != 'sysadmin/dummy' and projectPath != 'sysadmin/.github' and (projectPath.startswith('sysadmin/') or projectPath.startswith('websites/')):
    print("No need to sync to GitHub - this is a sysadmin/website repository")
    sys.exit(0)

# Is the project one that is deliberately excluded?
if projectPath == 'graphics/digikam':
    print("Not syncing to GitHub - this project cannot be pushed due to content that GitHub will not accept")
    sys.exit(0)

# Retrieve the project so that we can begin the replication process to GitHub
ourProject = knownProjects[ projectPath ]

# Establish our connection to GitHub.com
githubServer = github.Github( args.token )
githubOrg    = githubServer.get_organization( args.organization )

# Check whether the repository exists on GitHub
# If it doesn't exist, then create it
try:
    githubRepo = githubOrg.get_repo( ourProject['identifier'] )
except:
    githubRepo = githubOrg.create_repo( name=ourProject['identifier'], auto_init=False )

# Prepare the repository description to fit GitHub's requirements
# In particular, the block control characters (such as newlines) so we eliminate those
projectDescription = ourProject['description'].split("\n")
githubDescription = projectDescription[0]

# Now that the repository has been retrieved (or created if needed) we sync across any settings we need to sync
githubRepo.edit(
    description=githubDescription,
    private=False,
    has_issues=False,
    has_projects=False,
    has_wiki=False,
    has_downloads=False
)

# Do we have topic tags that need to be synced up to GitHub?
if 'topics' in ourProject:
    # Make sure the topics are acceptable to GitHub
    topics = [ topic.lower() for topic in ourProject['topics'] ]
    # Submit the cleaned topics to GitHub
    githubRepo.replace_topics( topics )

# Determine where we will find the script that will do the sync up to GitHub
scriptLocation = os.path.dirname( os.path.realpath(__file__) )

# With all of that done we can now sync the repository we have locally up to GitHub...
commandToRun = 'python3 {scriptLocation}/update-repo-mirror.py "{repositoryUrl}"'
commandToRun = commandToRun.format( scriptLocation=scriptLocation, repositoryName=ourProject['identifier'], repositoryUrl=githubRepo.ssh_url )
process = subprocess.Popen( commandToRun, stdout=sys.stdout, stderr=sys.stderr, shell=True, cwd=os.getcwd() )
process.wait()

# And we're done!
print("Sync to GitHub successful!")
sys.exit(0)
